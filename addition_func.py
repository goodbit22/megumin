from ipaddress import ip_address
from subprocess import Popen, PIPE
from nmap3 import Nmap
from socket import socket, AF_INET, SOCK_STREAM
from logging import info, warning


def insert_ipAddress():
    def check_ip(address):
        process = Popen(['ping', '-c', '2', address], stderr=PIPE, stdout=PIPE)
        response = process.wait()
        return response
    status = None
    while status != 0:
        while True:
            try:
                host = input("Please enter the ip address: ")
                ip_address(host)
                break
            except ValueError:
                print("Not a valid IP address")
        status = check_ip(host)
        if status == 0:
            print("Status: up ")
            return host
        else:
            print("Status: down")


def os_detection(address='127.0.0.1'):
    nmap = Nmap()
    return nmap.nmap_os_detection(address)


def port_available(address, number_port):
    s = socket(AF_INET, SOCK_STREAM)
    port = s.connect_ex((address, number_port))
    if port == 0:
        s.close()
        info(f"[+] Port {number_port} is open")
        return True
    else:
        s.close()
        warning(f"[-] Port {number_port} is not open")
        return False